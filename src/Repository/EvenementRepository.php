<?php

namespace App\Repository;

use App\Entity\Evenement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Evenement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Evenement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Evenement[]    findAll()
 * @method Evenement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvenementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Evenement::class);
    }
    public function getByUser($user){
        return $this->createQueryBuilder("e")
            ->andWhere("e.user = :val")
            ->setParameter("val", $user)
            ->getQuery()
            ->getResult();
    }

    public function getEveCategorie($value)
    {
        $qb=$this->createQueryBuilder('e'); // il lui faut une lettre, i : item en base de données donc schéma
        $qb->select('c.libelle','e.nom', 'e.prix', 'e.description', 'e.photo', 'e.nombrePlaces')
            ->join('App:Categorie','c')
            ->where('e.categorie=c.id')
            ->andWhere('c.libelle = :val')
            ->setParameter('val',$value)
            ->addOrderBy('c.libelle', 'ASC');
        return $qb->getQuery()->getResult();
    }


    public function getPlaceAchete()
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery("SELECT e.nom, e.id, c.libelle, e.prix, e.nombrePlaces, sum(p.quantite) as placeAchete, e.photo
                                    FROM App:Evenement e 
                                    LEFT JOIN e.places p
                                    JOIN e.categorie c
                                    GROUP BY e.id");
;
        return $query->getResult();
    }


    // /**
    //  * @return Evenement[] Returns an array of Evenement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Evenement
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
