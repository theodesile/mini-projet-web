<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends EntityRepository implements UserLoaderInterface
{

    public function loadUserByUsername($email)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $email)
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getCommandePanier(){
        $qb=$this->createQueryBuilder('e'); // il lui faut une lettre, i : item en base de données donc schéma
        $qb->select('c.libelle','e.nom', 'e.prix', 'e.description', 'e.photo', 'e.disponible')
            ->join('App:Categorie','c')
            ->where('e.categorie=c.id')
            ->addOrderBy('c.libelle', 'ASC');
        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
   public function getEveCategorie()
    {
        $qb=$this->createQueryBuilder('e'); // il lui faut une lettre, i : item en base de données donc schéma
        $qb->select('c.libelle','e.nom', 'e.prix', 'e.description', 'e.photo', 'e.disponible')
            ->join('App:Categorie','c')
            ->where('e.categorie=c.id')
            ->addOrderBy('c.libelle', 'ASC');
        return $qb->getQuery()->getResult();
    }

*/
    public function findByRole($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.roles = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(20)
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
