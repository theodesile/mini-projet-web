<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\InscriptionType;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Twig\Environment;

/**
 * @package App\Controller
 * @Security("has_role('ROLE_ADMIN')")
 */

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    /**
     * @Route("/showUser", name="user.show")
     */
    public function showUser(RegistryInterface $doctrine,Environment $twig){
        $user=$doctrine->getRepository(User::class)->findByRole('ROLE_CLIENT');
        return new Response($twig->render('backOff/User/showUser.html.twig', ['user' => $user]));
    }

    /**
     * @Route("/user/delete", name="user.delete", methods={"DELETE"})
     */
    public function deleteUser(Request $request, Environment $twig, RegistryInterface $doctrine, FormFactoryInterface $formFactory)
    {
        $user=$doctrine->getRepository(User::class)->find($request->query->get('id'));
        $doctrine->getEntityManager()->remove($user);
        $doctrine->getEntityManager()->flush();
        $this->addFlash('notice','Utilisateur supprimé !');
        return $this->redirectToRoute('user.show');
    }

    /**
     * @Route("/editUser", name="user.edit", methods={"PUT"})
     */
    public function editUser(Request $request, RegistryInterface $doctrine, FormFactoryInterface $formFactory,UserPasswordEncoderInterface $encoder)
    {
        $user=$doctrine->getRepository(User::class)->find($request->query->get('id'));
        $form=$formFactory->createBuilder(InscriptionType::class,$user,  array(
            //'action' => $this->generateUrl('target_route'),
            'method' => 'PUT',
        ))->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user,$user->getPassword());
            $user->setPassword($hash);
            $doctrine->getEntityManager()->flush();
            $this->addFlash('notice','Utilisateur modifié !');
            return $this->redirectToRoute('user.show');
        }
        return $this->render('backOff/user/edit.html.twig',['form'=>$form->createView()]);
    }

    /**
     * @Route("/inscription", name="user.inscription", methods={"POST"})
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder,AuthenticationUtils $authUtils, RegistryInterface $doctrine){
        $user = new User();
        $form= $this->createForm(InscriptionType::class, $user);
        $form->handleRequest($request);
        $user->setRoles('ROLE_CLIENT');
        if($form->isSubmitted() && $form->isValid()){
            $hash = $encoder->encodePassword($user,$user->getPassword());
            $user->setPassword($hash);
            $manager->persist($user);
            $manager->flush();
            $this->addFlash('notice','Utilisateur inscrit !');
            $utilisateur = $doctrine->getRepository(User::class)->findByRole('ROLE_CLIENT');
            return $this->render('backOff/user/showUser.html.twig',['user'=>$utilisateur]);
        }
        return $this->render('backOff/user/inscription.html.twig', [
            'form'=>$form->createView()
        ]);
    }
}
