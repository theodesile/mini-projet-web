<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Evenement;
use App\Form\CategorieType;
use App\Repository\CategorieRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class CategorieController extends AbstractController
{
    /**
     * @Route("/categorie", name="categorie")
     */
    public function index()
    {
        return $this->render('categorie/index.html.twig', [
            'controller_name' => 'CategorieController',
        ]);
    }

    /**
     * @Route("/showConference", name="Panier.filtreConference")
     */
    public function showByConference(Environment $twig, RegistryInterface $doctrine){
        $events = $doctrine->getRepository(Evenement::class)->getEveCategorie('conference');
        return new Response($twig->render('categorie/categorie.html.twig',['events' =>$events]));
    }

    /**
     * @Route("/showConcert", name="Panier.filtreConcert")
     */
    public function showByConcert(Environment $twig, RegistryInterface $doctrine){
        $events = $doctrine->getRepository(Evenement::class)->getEveCategorie('concert');
        return new Response($twig->render('categorie/categorie.html.twig',['events' =>$events]));
    }

    /**
     * @Route("/showTheatre", name="Panier.filtreTheatre")
     */
    public function showByTheatre(Environment $twig, RegistryInterface $doctrine){
        $events = $doctrine->getRepository(Evenement::class)->getEveCategorie('théatre');
        return new Response($twig->render('categorie/categorie.html.twig',['events' =>$events]));
    }

    /**
     * @Route("/showDanse", name="Panier.filtreDanse")
     */
    public function showByDanse(Environment $twig, RegistryInterface $doctrine){
        $events = $doctrine->getRepository(Evenement::class)->getEveCategorie('danse');
        return new Response($twig->render('categorie/categorie.html.twig',['events' =>$events]));
    }

    /**
     * @Route("/showDivers", name="Panier.filtreDivers")
     */
    public function showByDivers(Environment $twig, RegistryInterface $doctrine){
        $events = $doctrine->getRepository(Evenement::class)->getEveCategorie('divers');
        return new Response($twig->render('categorie/categorie.html.twig',['events' =>$events]));
    }

    /**
     * @Route("/categorie/add", name="categorie.add", methods={"GET","POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addCategorie(Request $request, Environment $twig, RegistryInterface $doctrine, FormFactoryInterface $formFactory){
        $form=$formFactory->createBuilder(CategorieType::class)->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $categorie=$form->getData();
            $doctrine->getEntityManager()->persist($categorie);
            $doctrine->getEntityManager()->flush();
            $this->addFlash('notice','Catégorie ajouté !');
            return $this->redirectToRoute('Categorie.admin');
        }
        return new Response($twig->render('backOff/categoriee/addCategorie.html.twig',['form'=>$form->createView()]));
    }

    /**
     * @Route("/categorie/delete", name="categorie.delete", methods={"DELETE"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteCategorie(Request $request, Environment $twig, RegistryInterface $doctrine, FormFactoryInterface $formFactory)
    {
        $categorie=$doctrine->getRepository(Categorie::class)->find($request->query->get('id'));
        $doctrine->getEntityManager()->remove($categorie);
        $doctrine->getEntityManager()->flush();
        $this->addFlash('notice','Catégorie supprimé !');
        return $this->redirectToRoute('Categorie.admin');
    }

    /**
     * @Route("/categorie/edit", name="categorie.edit", methods={"PUT"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editCategorie(Request $request, Environment $twig, RegistryInterface $doctrine, FormFactoryInterface $formFactory)
    {
        $categorie=$doctrine->getRepository(Categorie::class)->find($request->query->get('id'));
        $form=$formFactory->createBuilder(CategorieType::class,$categorie, array('method'=>'PUT'))->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $doctrine->getEntityManager()->flush();
            $this->addFlash('notice','Categorie modifié !');
            return $this->redirectToRoute('Categorie.admin');
        }
        return new Response($twig->render('backOff/categoriee/addCategorie.html.twig',['form'=>$form->createView()]));
    }

    /**
     * @Route("/showCategorieAdmin", name="Categorie.admin")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showByCategorieAdmin(Environment $twig, RegistryInterface $doctrine){
        $events = $doctrine->getRepository(Categorie::class)->findAll();
        return new Response($twig->render('backOff/categoriee/showCategorie.html.twig',['events' =>$events]));
    }


}
