<?php


namespace App\Controller;
use App\Entity\Commande;
use App\Entity\Etat;
use App\Entity\PanierPlace;
use App\Entity\Place;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


use App\Entity\Evenement;
use App\Entity\User;
use App\Form\EvenementType;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;

use DateTime;
use Symfony\Component\Validator\Constraints\Date;
use Twig\Environment;                            // template TWIG
use Symfony\Bridge\Doctrine\RegistryInterface;   // ORM Doctrine
use Symfony\Component\HttpFoundation\Request;    // objet REQUEST

class CommandController extends AbstractController
{
    /**
     * @Route("/showCommand", name="Command.show")
     */
    public function showCommand(Request $request, Environment $twig, RegistryInterface $doctrine){
        $user=$this->getUser();
        $commands=$doctrine->getRepository(Commande::class)->findBy(["user"=>$user]);
        $prixTotal=array();
        $i=0;
        foreach ($commands as $command){
            array_push($prixTotal, 0);
            foreach ($command->getPlaces() as $place){
                $prixTotal[$i]+=$place->getQuantite()*$place->getPrix();
            }
            $i+=1;
        }
        return new Response($twig->render('frontOff/showCommand.html.twig',["commands" => $commands, "prixTotal" => $prixTotal]));
    }

    /**
     * @Route("/showCommande", name="gestion.commande.show")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showCommandeAdmin(Environment $twig, RegistryInterface $doctrine){
        $commands=$doctrine->getRepository(Commande::class)->findAll();
        $prixTotal=array();
        $i=0;
        foreach ($commands as $command){
            array_push($prixTotal, 0);
            foreach ($command->getPlaces() as $place){
                $prixTotal[$i]+=$place->getQuantite()*$place->getPrix();
            }
            $i+=1;
        }
        return new Response($twig->render('backOff/showCommand.html.twig',["commands" => $commands, "prixTotal" => $prixTotal]));
    }

    /**
     * @Route("/expedie/{id}",name="gestion.commande.expedie", methods={"PUT"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function expedie(RegistryInterface $doctrine, $id){
        $commande=$doctrine->getRepository(Commande::class)->find($id);
        $etat=$doctrine->getRepository(Etat::class)->findAll()[1];
        $commande->setEtat($etat);
        $doctrine->getEntityManager()->flush();
        return $this->redirectToRoute('gestion.commande.show');
    }
    /**
     * @Route("/showDetailsCommand/{id}",name="Command.showDetails", methods={"GET"})
     */
    public function showDetails(RegistryInterface $doctrine, $id, Environment $twig){
        $user=$this->getUser();
        $commande=$doctrine->getRepository(Commande::class)->find($id);
        if($commande==NULL){
            return $this->redirectToRoute("Panier.show");
        }
        if($commande->getUser() != $user){
            return $this->redirectToRoute("Panier.show");
        }
        return new Response($twig->render('frontOff/showDetailsCommand.html.twig',["places" => $commande->getPlaces()]));
    }

    /**
     * @Route("/showDetailCommande/{id}", name="Commande.showDetail")
     */
    public function showDetail(RegistryInterface $doctrine,$id, Environment $twig){
        $commande=$doctrine->getRepository(Commande::class)->find($id);
        return new Response($twig->render('backOff/showDetailCommande.html.twig',["places" => $commande->getPlaces()]));
    }
}