<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Entity\Evenement;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Twig\Environment;                            // template TWIG
use Symfony\Bridge\Doctrine\RegistryInterface;   // ORM Doctrine
use Symfony\Component\HttpFoundation\Request;    // objet REQUEST
use Symfony\Component\HttpFoundation\Response;    // objet RESPONSE
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormFactoryInterface;

class CommentaireController extends AbstractController
{
    /**
     * @Route("/commentaire/{id}", name="Commentaire.show")
     */
    public function showCommentaire(Request $request, Environment $twig, RegistryInterface $doctrine, $id)
    {
        $event =$doctrine->getRepository(Evenement::class)->find($id);
        dump("id : " .$id);
        dump($event);
        $form = $this->createFormBuilder(new Commentaire())
            ->add("contenu", TextType::class)
            ->add("note", TextType::class)
            ->add('submit', SubmitType::class)
        ->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $commentaire=$form->getData();
            $commentaire->setUtilisateur($this->getUser());
            $commentaire->setEvenement($doctrine->getRepository(Evenement::class)->find($id));
            $doctrine->getEntityManager()->persist($commentaire);
            $doctrine->getEntityManager()->flush();
            dump($commentaire);
        }
        $commentaires=$doctrine->getRepository(Commentaire::class)->findBy(["evenement"=>$event]);
        if($this->hasCommandThat($doctrine, $event)) {
            return $this->render('commentaires/showCommentaires.html.twig', [
                'commentaires' => $commentaires, "event"=>$event , 'form' => $form->createView()
            ]);
        }
        return $this->render('commentaires/showCommentaires.html.twig', [
            'commentaires' => $commentaires, "event"=>$event
        ]);
    }
    public function hasCommandThat(RegistryInterface $doctrine, $event){
        $user=$this->getUser();
        foreach ($user->getCommandes() as $command){
            foreach ($command->getPlaces() as $place){
                if($place->getEvenement()->getId() == $event->getId())
                    return true;
            }
        }
        return false;
    }


    /**
     * @Route("/show/commentaire", name="Comment.show")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showComment(Request $request, Environment $twig, RegistryInterface $doctrine){
        $commentaire=$doctrine->getRepository(Commentaire::class)->findAll();
        // dump($doctrine->getRepository(Evenement::class)->);   getPlaceAchete()
        return new Response($twig->render('backOff/Commentaire/showCommentaire.html.twig', ['commentaire' => $commentaire]));
    }

    /**
     * @Route("/comment/delete", name="Commentaire.delete", methods={"DELETE"})
     */
    public function deleteEvenement(Request $request, Environment $twig, RegistryInterface $doctrine, FormFactoryInterface $formFactory)
    {
        $commentaire=$doctrine->getRepository(Commentaire::class)->find($request->query->get('id'));
        $doctrine->getEntityManager()->remove($commentaire);
        $doctrine->getEntityManager()->flush();
        $this->addFlash('notice','Commentaire supprimé !');
        return $this->redirectToRoute('Comment.show');
    }
    /*

     */
}
