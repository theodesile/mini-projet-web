<?php


namespace App\Controller;
use App\Entity\Commande;
use App\Entity\Etat;
use App\Entity\PanierPlace;
use App\Entity\Place;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


use App\Entity\Evenement;
use App\Entity\User;
use App\Form\EvenementType;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;

use DateTime;
use Symfony\Component\Validator\Constraints\Date;
use Twig\Environment;                            // template TWIG
use Symfony\Bridge\Doctrine\RegistryInterface;   // ORM Doctrine
use Symfony\Component\HttpFoundation\Request;    // objet REQUEST

class PanierController extends AbstractController
{
    /**
     * @Route("/showPanier", name="Panier.show")
     */
    public function showPanier(Request $request, Environment $twig, RegistryInterface $doctrine){
        $user = $this->getUser();
        $panier = $doctrine->getRepository(PanierPlace::class)->findBy(["user" => $user]);
        $events = $doctrine->getRepository(Evenement::class)->findAll();
        return new Response($twig->render('frontOff/frontOFFICE.html.twig',["panier" => $panier, "events"=>$events]));
    }

    /**
     * @Route("/actualise/{id}",name="ajoute_quantite", methods={"PUT"})
     */
    public function ajouteQuantite(Request $request, RegistryInterface $doctrine,$id){
        $user = $this->getUser();
        $event=$doctrine->getRepository(Evenement::class)->find($id);
        $panier = $doctrine->getRepository(PanierPlace::class)->findBy(["evenement" => $event])[0];
        $panier->setQuantite($panier->getQuantite()+1);
        $doctrine->getEntityManager()->flush();
        return $this->redirectToRoute('Panier.show');
    }

    /**
     * @Route("/actualisee/{id}",name="soustrait_quantite", methods={"PUT"})
     */
    public function soustraitQuantite(Request $request, RegistryInterface $doctrine,$id){
        $user = $this->getUser();
        $event=$doctrine->getRepository(Evenement::class)->find($id);
        $panier = $doctrine->getRepository(PanierPlace::class)->findBy(["evenement" => $event])[0];
        $pan=$panier->getQuantite()-1;
        $panier->setQuantite($pan);
        if ($pan==0){
            $doctrine->getEntityManager()->remove($panier);
            $doctrine->getEntityManager()->flush();
            return $this->redirectToRoute('Panier.show');
        } else {
            $doctrine->getEntityManager()->flush();
            return $this->redirectToRoute('Panier.show');
        }
    }

    /**
     * @Route("/delete/panier/{id}",name="Panier.delete", methods={"DELETE"})
     */
    public function deletePanier(Request $request, RegistryInterface $doctrine,$id){
        $panierPlace=$doctrine->getRepository(PanierPlace::class)->find($id);
        $doctrine->getEntityManager()->remove($panierPlace);
        $doctrine->getEntityManager()->flush();
        $this->addFlash('notice','Evenement supprimé du panier!');
        return $this->redirectToRoute('Panier.show');
    }
    public function emptyPanier(RegistryInterface $doctrine, $user){
        $panier = $doctrine->getRepository(PanierPlace::class)->findBy(["user"=>$user]);
        foreach ($panier as $panierPlace){
            $doctrine->getEntityManager()->remove($panierPlace);
        }
        $doctrine->getEntityManager()->flush();
    }
    /**
     * @Route("/requestValidCommand",name="Panier.requestValidCommand", methods={"POST"})
     */
    public function requestValidCommand(Request $request, RegistryInterface $doctrine,Environment $twig){
        $user = $this->getUser();
        $panier = $doctrine->getRepository(PanierPlace::class)->findBy(["user" => $user]);
        return new Response($twig->render('frontOff/requestValidPanier.html.twig',["panier" => $panier]));
    }

    /**
     *@Route("/validCommad",name="Panier.validCommand", methods={"POST"})
     */
    public function validCommand(Request $request, RegistryInterface $doctrine,Environment $twig){
        $user = $this->getUser();
        $panier = $doctrine->getRepository(PanierPlace::class)->findBy(["user"=>$user]);
        $commande = new Commande();
        $etat = new Etat();
        $commande->setDate(new DateTime());
        $etat=$doctrine->getRepository(Etat::class)->findAll()[0];
        $commande->setUser($user);
        $commande->setEtat($etat);
        $doctrine->getEntityManager()->persist($commande);
        foreach ($panier as $panierPlace){
            $place = new Place();
            if($panierPlace->getEvenement()->getPrix()!=null)
                $place->setPrix($panierPlace->getEvenement()->getPrix());
            else{
                $place->setPrix("0");
            }
            $place->setQuantite($panierPlace->getQuantite());
            if($panierPlace->getEvenement()->getNombrePlaces()-$panierPlace->getQuantite()<0){
                $this->addFlash('notice',"Le nombre de place inscrit n'est pas disponible");
                return $this->redirectToRoute("Panier.show");
            }
            if($panierPlace->getEvenement()->getNombrePlaces()-$panierPlace->getQuantite()==0){
                $panierPlace->getEvenement()->setDisponible(0);
            }
            $panierPlace->getEvenement()->setNombrePlaces($panierPlace->getEvenement()->getNombrePlaces()-$panierPlace->getQuantite());
            //$commande->addPlace($place)
            $place->setCommande($commande)
                ->setEvenement($panierPlace->getEvenement());
            $doctrine->getEntityManager()->persist($place);
        }
        $doctrine->getEntityManager()->flush();
        $this->emptyPanier($doctrine, $user);
        return $this->redirectToRoute("Command.show");
    }
    /**
     * @Route("/panier/add/{id}", name="Panier.add", methods={"POST"})
     */
    public function addPanier(Request $request, Environment $twig, RegistryInterface $doctrine,$id){
        $user = $this->getUser();
        $event=$doctrine->getRepository(Evenement::class)->find($id);
        $estRempli = $doctrine->getRepository(PanierPlace::class)->findBy(["evenement" => $event]);
        $donnes['quantite']=0;
        $d=date("d/m/Y");
        $date= DateTime::createFromFormat("d/m/Y",$d);
        if(empty($estRempli)) {
            $panier = new PanierPlace();
            $panier->setUser($user);
            $panier->setEvenement($event);
            $panier->setDateAchat($date);
            $panier->setQuantite(1);
            $doctrine->getEntityManager()->persist($panier);
            $doctrine->getEntityManager()->flush();
            $this->addFlash('notice','Evenement ajouté au panier!');

        } else{
            $panier = $doctrine->getRepository(PanierPlace::class)->findBy(["evenement" => $event])[0];
            $panier->setQuantite($panier->getQuantite()+1);
            $doctrine->getEntityManager()->flush();

        }

        return $this->redirectToRoute('Panier.show');
    }





}
