<?php
// src/Controller/SecurityController.php
namespace App\Controller;
use ReCaptcha\ReCaptcha;
use App\Entity\User;
use App\Form\InscriptionType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\SecurityBundle\Tests\Functional\Bundle\CsrfFormLoginBundle\Form\UserLoginType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Twig\Environment;


class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authUtils)
    {
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    /**
     * @Route("/registration", name="user_registration",methods={"GET","POST"})
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder, AuthenticationUtils $authUtils)
    {
        $user = new User();
        $form = $this->createForm(InscriptionType::class, $user);
        $form->handleRequest($request);
        $user->setRoles('ROLE_CLIENT');
        if ($form->isSubmitted() && $form->isValid()) {
            $recaptcha = new \ReCaptcha\ReCaptcha('6Ldd4cEUAAAAADAGPTPXHdUszai86Z5pwpFuXtFM');
            $resp = $recaptcha->setExpectedHostname('127.0.0.1')
                ->verify($_POST['g-recaptcha-response']);
            if (isset($_POST['g-recaptcha-response'])) {
                if ($resp->isSuccess()) {
                    $hash = $encoder->encodePassword($user, $user->getPassword());
                    $user->setPassword($hash);
                    $manager->persist($user);
                    $manager->flush();
                    $this->addFlash('notice', 'Inscription validé !');

                    return $this->redirectToRoute('login');
                } else {

                    $this->addFlash('notice', 'Donnée modifiié !');
                    return $this->render('security/registration.html.twig', [
                        'form' => $form->createView()
                    ]);
                }
            }
        }
        $this->addFlash('notice', 'Donnée modifiié !');
        return $this->render('security/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/change/{id}", name="user_registration_complete", methods={"PUT"})
     */
    public function editDonnees(Request $request, RegistryInterface $doctrine, FormFactoryInterface $formFactory, $id, UserPasswordEncoderInterface $encoder)
    {
        $user = $doctrine->getRepository(User::class)->find($id);
        $form = $formFactory->createBuilder(InscriptionType::class, $user, array(
            //'action' => $this->generateUrl('target_route'),
            'method' => 'PUT',
        ))->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $doctrine->getEntityManager()->flush();
            $this->addFlash('notice', 'Donnée modifiié !');
        }
        return $this->render('security/registration.html.twig', ['form' => $form->createView()]);
    }

}